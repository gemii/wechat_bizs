# encoding=utf8
# author=spenly
# mail=i@spenly.com

from tools.messager import Messager

# save log
# r/w db
# task manager, pub/sub
#

"""
# 好友
contacts = {
    "@******": {
        "Name": "",
        # ...
    }
}
# 群聊
groups = {
    "@@****": {
        "RoomID": "",
        "Name": "...",
        # ...
    }
}
# 群成员
group_members = {
    "@******": {
        "Name": "",
        # ...
        "display_name": {"@@*****": "a1", "@@": ""},
        "member_id": {"@@*****": "b1", "@@": ""}
    }
}
# 群和群成员关系
group_members_relation = {
    "@@***": ["@***", "@****"]
}
# 机器人信息
info = {
    "role":1
}
"""

messager = Messager("127.0.0.1")


def main():
    while True:
        msg = messager.receive()
        # add code here to handle msg
        messager.send(msg)


if __name__ == "__main__":
    main()
