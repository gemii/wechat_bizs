#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import traceback
import copy

from tools import Constant
from tools import cm
from tools import Log
from tools.memory_db import memory_db
from tools.mysql_db import MysqlDB
from tools.utils import *


class WechatHandle(object):

    def __init__(self):
        self.db = MysqlDB(cm.mysql())

    def get_robot_role(self, robot_id):
        """
        从内存中读取机器人角色
        获取键值不存在时则去DB查询:
            存在:添加内存
            不存在:设置默认角色
        :param robot_id: 机器人编号
        :return: 角色
        """
        robot_role = memory_db.get_data(robot_id, Constant.ROBOT_INF, 'role')
        if robot_role:
            return robot_role
        else:
            result = self.db.select(Constant.TABLE_ROBOT_ROLE, ['Role'], {'BotTag': robot_id})
            if result:
                robot_role = result[0]['Role']
            else:
                robot_role = Constant.RoBOt_ROLE_DEFAULT
            # 将机器人角色信息写入内存
            memory_db.set_data(robot_role, Constant.ROBOT_INFO, 'role', robot_role)
            return robot_role

    def receive_msg_handle(self, msg):
        """
        处理从任务队列获取的消息
        :param msg: 消息 String 类型
        :return:
        """
        msg = json.loads(msg)
        if not msg:
            return False

        if not msg['CallBackMsgId']:  # 非回调消息
            self.handle_not_call_back_msg(msg)
        else:  # 回调消息
            self.handle_call_back_msg(msg)

    def handle_not_call_back_msg(self, msg):
        """
        处理非回调消息
        :param msg: 消息 Dict 类型
        :return:
        """
        if msg['Type'] == Constant.TASK_MSG_TYPE_WECHAT_MSG:  # 微信消息
            self.handle_mod(msg)
            self.handle_msg(msg)
        elif msg['Type'] == Constant.TASK_MSG_TYPE_INIT_CONTACTS:  # 初始化联系人列表
            self.handle_init_contacts(msg)


    def handle_call_back_msg(self, msg):
        """
        处理回调消息
        :param msg: 消息 Dict 类型
        :return:
        """

    def handle_init_contacts(self, msg):
        """
        处理初始化联系人列表
        :param msg: 联系人信息 Dict 类型
        :return:
        """
        contacts = {}  # 好友
        groups = {}  # 群聊
        for contact in msg['MemberList']:
            if contact['UserName'] in Constant.API_SPECIAL_USER:  # 特殊账号
                continue
            elif contact['VerifyFlag'] & 8 != 0:  # 公众号/服务号
                continue
            elif contact['UserName'].startswith('@@'):  # 群聊
                groups[contact['UserName']] = contact
            elif contact['UserName'].startswith('@'):  # 好友
                contacts[contact['UserName']] = contact

        # 获取群聊的RoomID,更新机器人和群聊的对应关系
        self.update_robot_group_list(msg['RobotID'], groups, msg['CreateTime'])

        # 获取Bot角色
        robot_role = self.get_robot_role(msg['BotId'])

        if robot_role == Constant.ROBOT_ROLE_SPIDER:  # 爬数机器人处理方法
            self.handle_spider_robot_init_contacts(contacts, groups)
        # 将members 和 groups 保存

    def update_robot_group_list(self, robot_id, groups, create_time):
        """
        获取群聊的RoomID & 更新机器人和群的关联表
        :param robot_id: 机器人标识
        :param groups: 群聊列表
        :param create_time: 消息发送时间
        :return:
        """
        # 删除表中的原有群和机器人关联的记录
        self.db.delete(Constant.TABLE_ROBOT_GROUP_INFO, {'RobotID': robot_id})
        rows = []
        for group in groups:
            room_id = self.get_group_id_by_nickname(group)
            group['RoomID'] = room_id
            row = {
                'RobotID': robot_id,
                'RoomID': group['RoomID'],
                'RoomName': group['NickName'],
                'StartTime': format_time(create_time)
            }
            rows.append(row)
        self.db.insert_many(Constant.TABLE_ROOM_INFO, rows, True)

    def get_group_id_by_nickname(self, group):
        """
        根据群名获取群ID
        :param group: 群信息
        :return: RoomID
        """
        sql = """
            select
                RoomID
            from %s
            where
            NowRoomName='%s' or RoomName='%s'
            """ % (Constant.TABLE_ROBOT_GROUP_INFO_NAME, group['NickName'], group['NickName'])
        group_info = self.db.exec_sql(sql)
        if group_info:
            return group_info[0]['RoomID']
        else:
            Log.general_log.info('[%s]群在数据库中无法匹配RoomID' % group['NickName'])
            return group['UserName']

    def send_fetch_group_contacts_task(self, groups):
        """
        发送获取群聊信息任务(包括群成员部分信息)
        :param groups: 要获取的群聊
        :return:
        """

    def handle_spider_robot_init_contacts(self, contacts, groups):
        """
        处理爬数机器人初始化联系人列表
        :param contacts: 好友
        :param groups: 群聊
        :return:
        """


    def handle_mod(self, msg):
        """
        处理微信联系人变动
        :param msg: 消息内容 Dict 类型
        :return:
        """
        try:
            for mod_contact in msg['Content']['ModContactList']:
                if mod_contact['UserName'].startswith('@@'):  # group
                    group = memory_db.get_data(msg['RobotID'], Constant.ROBOT_GROUP, mod_contact['UserName'])
                    if group:  # 群聊已经群在
                        # 处理群聊变动
                        mod_contact['RoomID'] = group['RoomID']
                        self.handle_group_list_change([mod_contact])

                        # 处理群聊成员变动
                        self.handle_group_member_change(msg['RobotID'], mod_contact)
                    else:  # 新获取群聊
                        pass

        except:
            Log.error_log.error(traceback.format_exc())

    def handle_msg(self, msg):
        """
        处理微信消息
        :param msg: 消息内容 Dict 类型
        :return:
        """

    def handle_group_list_change(self, group_list):
        """
        @brief      handle group list & saved in DB
        @param      group_list  Array
        """
        try:
            for group in group_list:
                data = {
                    'currentCount': group['MemberCount'],
                    'totalCount': group['MemberCount']
                }
                self.db.update(Constant.TABLE_ROBOT_GROUP_INFO_NAME, data, {'RoomID': group['RoomID']})
        except:
            Log.error_log.error(traceback.format_exc())

    def handle_group_member_change(self, robot_id, group):
        """
        变动的群信息
        :param robot_id: 机器人编号
        :param group: 群信息
        :return:
        """
        robot_role = self.get_robot_role(robot_id)
        if robot_role == Constant.ROBOT_ROLE_SPIDER:  # 爬数机器人处理群成员变动
            self.handle_spider_group_member_change(robot_id, group)
        elif robot_role == Constant.ROBOT_ROLE_HELPER:  # 小助手机器人处理群成员变动
            pass
        else:
            pass

    def handle_spider_group_member_change(self, robot_id, group):
        """
        处理爬数机器人群成员变动
        :param robot_id: 机器人编号
        :param group: 群信息
        :return:
        """
        file_name = ''.join(group['UserName'], ".json")
        save_json(file_name, group['MemberList'])

        self.handle_contact_is_group_member(robot_id, group)

    def handle_contact_is_group_member(self, robot_id, group):
        """
        处理好友在群成员列表中,避免好友备注影响获取群成员昵称
        :param robot_id: 机器人编号
        :param group: 群信息
        :return:
        """
        for member in group['MemberList']:
            contact = memory_db.get_data(robot_id, Constant.ROBOT_CONTACT, member['UserName'])
            # 如果某群成员是机器人的好友, 若该好友为设置群备注,好友的备注会默认变成群成员昵称,影响机器人匹配群成员ID
            if contact and contact['NickName'] != member['NickName']:
                member['NickName'] = contact['NickName']

    def handle_have_change_member(self, robot_id, group):
        """
        处理发送变化的群成员,包括新增,修改信息,退群
        :param robot_id: 机器人编号
        :param group: 群信息
        :return:
        """
        leave_member_list = memory_db.get_data(robot_id, Constant.ROBOT_GROUP_RELATION, group['UserName'])
        new_member_list = []
        for member in group['MemberList']:
            old_info = memory_db.get_data(robot_id, Constant.ROBOT_GROUP_MEMBER, member['UserName'])

            if not old_info:  # 新加群用户
                member['MemberID'] = self.get_new_member_id(member)
                member['Alias'] = ''
                new_member_list.append(member)

                old_info['display_name'][group['UserName']] = member['DisplayName']

    def compare_two_member(self, member1, member2):
        """
        比较两个群成员信息是否相同(除Uin外)
        @param member1: dict
        @param member2: dict
        @return: True or False
        """
        m1 = copy.copy(member1)
        m2 = copy.copy(member2)
        m1.pop('MemberID')
        m1.pop('Alias')
        return m1 != m2

    def get_new_member_id(self, member):
        """
        根据新用户信息获取ID
        @param member: dict 用户信息
        @return: string id
        """
        member_id = get_md5_value(member['NickName'] + '_' + str(member['AttrStatus']))[8:-8] + \
                    get_md5_value(get_now_time('%Y-%m-%d'))[8:-8]
        return member_id

if __name__=='__main__':
    wh = WechatHandle()
    print wh.robot_role