# coding=utf-8
'''
Created on 2013-11-21
@author: jzx
'''
import pymysql
from log import Log
import threading


class MysqlDB:
    def __init__(self, conf):
        self.config = {
            'host': conf['host'],
            'user': conf['user'],
            'password': conf['passwd'],
            'database': conf['database'],
            'charset': 'utf8mb4',  # 支持1-4个字节字符
            'cursorclass': pymysql.cursors.DictCursor
        }
        self.conn = pymysql.connect(**self.config)
        self.cursor = self.conn.cursor()
        self.lock = threading.Lock()

    def __del__(self):
        if self.cursor:
            self.cursor.close()
        if self.conn:
            self.conn.close()

    def _trans_special_char(self, str1):
        str1 = str(str1)
        return str1.replace("\\", "\\\\").replace("\"", "\\\"").replace("\'", "\\\'")

    def _get_and_where(self, where_dict):
        """
        format sql condition string
        :param where_dict:
        :return:
        """
        return " and ".join(["%s='%s'" % (k, self._trans_special_char(v)) for k, v in where_dict.iteritems()])

    def _get_set(self, set_dict):
        """
        format sql update set string
        :param set_dict:
        :return:
        """
        return ",".join(["%s='%s'" % (k, self._trans_special_char(v)) for k, v in set_dict.iteritems()])

    def _get_fields_and_values(self, data_dict):
        """
        format insert field and value string
        :param data_dict:
        :return:
        """
        fields = []
        values = []
        for k, v in data_dict.iteritems():
            fields.append(k)
            values.append("'%s'" % self._trans_special_char(v))
        return ",".join(fields), ",".join(values)

    def insert(self, table, data_dict, ignore=False):
        """
        insert a record
        :param table:
        :param data_dict:
        :param ignore:  insert ignore
        :return:  last row id , cur.lastrowid
        """
        strsql = (ignore and "insert ignore" or "insert") + " into %s (%s) value (%s)"
        fields, values = self._get_fields_and_values(data_dict)
        strsql = strsql % (table, fields, values)
        if not values:
            Log.sql_log.info("invalid parameter, sql >" + strsql)
            return None
        self.lock.acquire()
        try:
            self.conn.ping(True)
            self.cursor.execute(strsql)
            res = self.cursor.lastrowid
            self.conn.commit()
            return res
        except Exception, ex:
            Log.sql_log.debug(repr({"table": table, "dict": data_dict, "ignore": ignore}))
            Log.sql_log.debug(strsql)
            Log.sql_log.error("exec sql error:" + str(ex))
            return None
        finally:
            self.lock.release()

    def insert_many(self, table, data_dict_list, ignore=False):
        """

        :param table:
        :param data_dict_list:
        :return:
        """
        if not data_dict_list:
            return None
        fields = ",".join(data_dict_list[0].keys())
        values = [tuple(v.values()) for v in data_dict_list]
        strsql = (ignore and "insert ignore" or "insert") + " into %s (%s) value (%s)"
        strsql = strsql % (table, fields, ",".join(["%s"]*len(values[0])))
        try:
            self.lock.acquire()
            self.conn.ping(True)
            res = self.cursor.executemany(strsql, values)
            self.conn.commit()
            return res
        except Exception, ex:
            Log.sql_log.debug(repr({"table": table, "dict_list": data_dict_list, "ignore": ignore}))
            Log.sql_log.debug(repr(data_dict_list))
            Log.sql_log.error("insert many error:" + str(ex))
            return None
        finally:
            self.lock.release()

    def select(self, table, columns_list=[], where_dict={}):
        """
        select record
        :param table:
        :param columns_list:
        :param where_dict:
        :return: iter row & col
        """
        where = self._get_and_where(where_dict)
        fields = columns_list and ",".join(columns_list) or "*"
        strsql = "select %s from %s"
        strsql = where and (strsql + " where %s" % where) or strsql
        strsql = strsql % (fields, table)
        try:
            self.lock.acquire()
            self.conn.ping(True)
            self.cursor.execute(strsql)
            res = self.cursor.fetchall()
            self.conn.commit()
            return res
        except Exception, ex:
            Log.sql_log.debug(repr({"table": table, "where": where_dict, "fields": columns_list}))
            Log.sql_log.error("exec sql error:" + str(ex))
            return None
        finally:
            self.lock.release()

    def update(self, table, set_dict, where_dict):
        """

        :param table:
        :param set_dict:
        :param where_dict:
        :return: affected rows number
        """
        set = self._get_set(set_dict)
        where = self._get_and_where(where_dict)
        strsql = "update %s set %s where %s" % (table, set, where)
        if set and where:
            return self.exec_sql(strsql)
        else:
            Log.sql_log.debug(repr({"table": table, "set": set_dict, "where": where_dict}))
            Log.sql_log.debug("update error > " + strsql)
            return None

    def delete(self, table, where_dict):
        """

        :param table:
        :param where_dict:
        :return:
        """
        where = self._get_and_where(where_dict)
        strsql = "delete from %s where %s" % (table, where)
        if where:
            return self.exec_sql(strsql)
        else:
            Log.sql_log.debug(repr({"table": table, "where": where_dict}))
            Log.sql_log.debug("delete without where condition > " + strsql)
            return None

    def exec_sql(self, strsql):
        """
        write data, such as insert, delete, update
        :param strsql: string sql
        :return: affected rows number
        return 0 when errors
        """
        try:
            self.lock.acquire()
            self.conn.ping(True)
            res = self.cursor.execute(strsql)
            if strsql.strip().lower().startswith("select"):
                res = self.cursor.fetchall()
            self.conn.commit()
            return res
        except Exception, ex:
            Log.sql_log.debug(strsql)
            Log.sql_log.error("exec sql error:" + str(ex))
            return 0
        finally:
            self.lock.release()

if __name__ == "__main__":
    config = {
            'host': "localhost",
            'user': "root",
            'passwd': "root",
            'database': "wechat",
            'charset': 'utf8mb4',  # 支持1-4个字节字符
            'cursorclass': pymysql.cursors.DictCursor
    }

    mysql = MysqlDB(config)
    print repr(mysql.select("RobotRole"))