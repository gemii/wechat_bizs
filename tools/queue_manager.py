#!/usr/bin/env python
# -*- coding: utf-8 -*-

from Queue import Queue
from collections import defaultdict

'''
暂时会存储以下几个queue：
QUEUE_SIMULATION = 0    # 多线程使用
QUEUE_DATA = 1          # 多线程使用
SEND_MSG_QUEUE = 'send_msg_queue'
UPLOAD_FILE_QUEUE = 'upload_file_queue'
GROUP_CHANGE_QUEUES = 'group_change_queues'
'''


class QueueManager(object):
    def __init__(self):
        self._queues = defaultdict(lambda: Queue())

    def put(self, queue, item):
        self._queues[queue].put(item)

    def get(self, queue):
        return self._queues[queue].get()

    def empty(self, queue):
        return self._queues[queue].empty()


qm = QueueManager()

if __name__ == '__main__':
    qm = QueueManager()
    print qm
    qm.put('a', 'aaaa')
    print qm.empty('a')
    print qm.get('a')
    print qm.empty('a')
