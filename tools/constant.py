# encoding=utf8
# author=spenly
# mail=i@spenly.com

import ConfigParser

class Constant(object):

    # config file path
    TABLE_ROBOT_GROUP_INFO_NAME = None
    WECHAT_CONFIG_FILE = '/Users/yunfei.han/Dev/wechat_bizs/tools/wechat.conf'

    # basic variable
    msg_bot_id_key = "BotId"

    # basic msg queue
    msg_up_channel = "msg_up"
    msg_down_channel = "msg_down"

    # redis collection
    call_back_set = "call_back"

    # pub task
    task_id_key = "task_id"

    # config parser
    config = ConfigParser.ConfigParser()
    config.read("bizs.conf")
    # config.read("../bizs.conf") ## debug

    # task msg type
    TASK_MSG_TYPE_INIT_CONTACTS = 1000
    TASK_MSG_TYPE_WECHAT_MSG = 1001

    # special user
    API_SPECIAL_USER = [
        'newsapp', 'filehelper', 'weibo', 'qqmail',
        'fmessage', 'tmessage', 'qmessage', 'qqsync',
        'floatbottle', 'lbsapp', 'shakeapp', 'medianote',
        'qqfriend', 'readerapp', 'blogapp', 'facebookapp',
        'masssendapp', 'meishiapp', 'feedsapp', 'voip',
        'blogappweixin', 'brandsessionholder', 'weixin',
        'weixinreminder', 'officialaccounts', 'wxitil',
        'notification_messages', 'wxid_novlwrv3lqwv11',
        'gh_22b87fa7cb3c', 'userexperience_alarm',
    ]

    # DB table name
    TABLE_NAME_ROBOT_ROLE = 'RobotRole'
    TABLE_ROOM_INFO = "WeChatRoomInfo"
    TABLE_ROBOT_GROUP_INFO = "WeChatRobotGroupInfo"
    TABLE_RECORD_ENTER_GROUP = 'WeChatEnterGroupRecord'
    TABLE_RECORD_RENAME_GROUP = 'WeChatRenameGroupRecord'
    TABLE_ROOM_MSG_LOG = 'WeChatRoomMessage'
    TABLE_USER_STATUS = 'UserStatus'
    TABLE_USER_FREIND_STATUS = "UserFriendStatus"

    # robot role
    ROBOt_ROLE_DEFAULT = 1
    ROBOT_ROLE_SPIDER = 1
    ROBOT_ROLE_HELPER = 2
    ROBOT_ROLE_MONITOR = 3

    # memory db item
    ROBOT_CONTACT = 'contacts'
    ROBOT_GROUP = 'groups'
    ROBOT_GROUP_MEMBER = 'group_members'
    ROBOT_GROUP_RELATION = 'group_members_relation'
    ROBOT_INFO = 'info'


    # aws access & secret key
    ACCESS_KEY = "AKIAOTDCELIZTI4INNVA"
    SECRET_KEY = "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"
    AWS_REGION = "cn-north-1"

    # aws simple queue service
    RECEIVE_MSG_QUEUE_NAME = "gemii_msgs"
    RECEIVE_TASK_QUEUE_NAME = "gemii_tasks"

    # redis data set sort msg by time for queue msg order
    MQ_ORDER_SCORE_POOL_NAME = "gemii_mq_order_score_pool"
    MQ_ORDER_DATA_POOL_NAME = "gemii_mq_order_data_pool"
    MQ_ORDER_TIME_OUT_KEY_PREFIX = "MQOD#"
    MQ_ORDER_SCORE_POOL_MAX_SIZE = 1000
    MQ_ORDER_SCORE_POOL_MAX_TIME_GAP = 5