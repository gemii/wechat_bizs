# encoding=utf8
# author=spenly
# mail=i@spenly.com

from constant import Constant
from big_queue import BigQueue
import redis
import threading
import json


class Messager():
    """
    Make a messager with a BigQueue object
    """

    def __init__(self, redis_host, queue_name=""):
        self._redis = redis.StrictRedis(host=redis_host)
        self._queue = BigQueue(queue_name or Constant.RECEIVE_MSG_QUEUE_NAME,
                               Constant.ACCESS_KEY, Constant.SECRET_KEY, Constant.AWS_REGION)
        self.score_pool_name = Constant.MQ_ORDER_SCORE_POOL_NAME
        self.score_pool_max_size = Constant.MQ_ORDER_SCORE_POOL_MAX_SIZE
        self.score_pool_time_gap = Constant.MQ_ORDER_SCORE_POOL_MAX_TIME_GAP
        self.data_pool_name = Constant.MQ_ORDER_DATA_POOL_NAME
        try:
            self._redis.info()
            self._queue.count()
        except Exception, ex:
            ex_msg = "An error occur when init Messager\n" \
                     "check your redis host and queue name\n" + ex.message
            nex = Exception(ex_msg)
            raise nex

    def receive(self):
        # 从列表开头获取一个元素，如果列表是空则阻塞
        # 方法是线程安全的
        msg = self._queue.dequeue()
        flag = True
        while flag:
            if msg:
                flag = self.parse_msg(msg)
            else:
                flag = False
        return self.get_msg_from_sorted_pool()

    def send(self, msg, call_back=False):
        """
        send msg to API service
        :param msg:
            # msg body demo
            {
                "MsgId":"1725351978_1484106576",
                "BotId":"1725351978", # Constant.msg_bot_id_key
                "Type":1001,
                "Content":{},
                "CreateTime":1484106576
            }
        :param call_back: bool, True or False,  default False.
        :return:
        """
        if call_back:
            self._cache_task_for_call_back(msg)
        self._queue.inqueue(msg)

    def parse_msg(self, msg):
        """

        :param msg:
        :return:
        """
        result = True
        if not msg:
            result = False
            return result
        dict_msg = json.loads(msg.get_body())
        msg_id = dict_msg["MsgId"]
        create_time = dict_msg["CreateTime"]
        self._redis.zadd(self.score_pool_name, float(create_time), msg_id)
        time_gap = self._redis.zrange(self.score_pool_name, -1, -1) - self._redis.zrange(self.score_pool_name, 0, 0)
        if self._redis.zcard(self.score_pool_name) > self.score_pool_max_size or time_gap > self.score_pool_time_gap:
            result = False
        return result

    def get_msg_from_sorted_pool(self):
        if self._redis.zcard(self.score_pool_name) > 0:
            msg_id = self._redis.zrange(self.score_pool_name, 0, 0)
            self._redis.zrem(self.score_pool_name, msg_id)
            return self._redis.hget(self.data_pool_name, msg_id)
        else:
            return None

    def _cache_task_for_call_back(self, msg):
        """
        cache task
        :param msg:
        :return:
        """
        if Constant.msg_bot_id_key in msg and Constant.task_id_key in msg:
            task_id = msg[Constant.task_id_key]
            self._redis.hset(Constant.call_back_set, task_id, msg)

    def request_task(self, test):
        pass


####  test thread  ####
class GetThread(threading.Thread):
    def __init__(self, tn):
        threading.Thread.__init__(self)
        self.msgr = Messager("127.0.0.1")
        self.tn = tn

    def run(self):
        for i in range(0, 10):
            print self.tn, self.msgr.receive()["MsgId"]


if __name__ == "__main__":
    # MultiThread Test
    for i in range(0, 3):
        print "starting thread %d" % i
        t = GetThread(i)
        t.start()
