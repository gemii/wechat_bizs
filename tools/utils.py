#!/usr/bin/env python
# coding: utf-8

from constant import Constant

import json
import hashlib
import random
import time

def create_msg(bot_id, type, content, call_back_id=None):
    """
    根据参数创建消息
    :param bot_id: 机器人编号
    :param type: 消息类型
    :param content: 消息内容
    :param call_back_id: 回调消息ID
    :return: json类型数据
    """
    current_time = int(time.time()*1000)  # 十三位时间戳
    msg = {
        'MsgId': bot_id + '_' + str(current_time),
        'BotId': bot_id,
        'Type': type,
        'Content': content,
        'CreateTime': current_time,
        'CallBackId': call_back_id
    }
    return msg

def format_time(date, formart='%Y-%m-%d %H:%M:%S'):
    """
    将某个时间按照格式进行格式化
    @param date: 时间 毫秒值
    @param formart: 格式 默认:%Y-%m-%d %H:%M:%S
    @return: 格式化后的时间
    """
    if date == 0:
        return get_now_time()
    ltime = time.localtime(date)
    return time.strftime(formart, ltime)

def get_md5_value(src):
    """
    返回MD5值
    :param src: 字符串或者二进制流
    :return: MD5值
    """
    m5_v = hashlib.md5()
    m5_v.update(src)
    return m5_v.hexdigest()

def get_now_time(formart='%Y-%m-%d %H:%M:%S'):
    """
    获取指定格式的当前时间
    @param formart: 格式 默认:%Y-%m-%d %H:%M:%S
    @return: 格式化后的当前时间
    """
    return time.strftime(formart, time.localtime())

def get_random_num(length=4):
    """
    获取指定长度的随机数
    @param length: 长度 默认:4
    @return: 随机数
    """
    return ''.join([str(i) for i in random.sample(range(0, 9), length)])

def save_json(file_name, data):
    """
    保存json文件
    :param file_name: 文件名
    :param data: 文件内容
    :return:
    """
    # TODO 实现保存文件

def split_array(arr, n):
    """
    将列表按照指定长度切分
    :param arr: 列表
    :param n: 长度
    :return: 长度<=n的列表
    """
    for i in xrange(0, len(arr), n):
        yield arr[i:i + n]
