#!/usr/bin/env python
# coding: utf-8

# ===================================================
from constant import Constant
from config_manager import cm
# ---------------------------------------------------
import logging
import logging.config


# ===================================================

class WeChatLog(object):
    def __init__(self):
        self.sql_log = self.creat_log(cm, 'sql')
        self.api_log = self.creat_log(cm, 'api')
        self.general_log = self.creat_log(cm, 'general')
        self.business_log = self.creat_log(cm, 'business')
        self.error_log = self.creat_log(cm, 'error')
        self.other_log = self.creat_log(cm, 'other')

        # 需要过滤的模块
        self.filter_log(['requests', "urllib3"])

    def creat_log(self, cm, name='sql'):
        logger = logging.getLogger(name)
        if len(logger.handlers) == 0:
            LOG_FILE = cm.getpath('logdir') + '/wechat.log'
            logger.setLevel(level=cm.get('logger_WeChat', 'level'))
            fh = logging.FileHandler(LOG_FILE)
            format_str = '%(asctime)s %(filename)s[line:%(lineno)d] %(name)s %(levelname)s --> %(message)s'
            formatter = logging.Formatter(format_str)
            fh.setFormatter(formatter)
            fh.setLevel(level=cm.get('handler_fileHandler', 'level'))
            ch = logging.StreamHandler()
            ch.setLevel(level=cm.get('handler_consoleHandler', 'level'))
            # 定义handler的输出格式
            ch.setFormatter(formatter)

            logger.addHandler(fh)
            logger.addHandler(ch)

        return logger

    def filter_log(self, names):
        for name in names:
            logger = logging.getLogger(name)
            logger.setLevel(logging.ERROR)


Log = WeChatLog()
