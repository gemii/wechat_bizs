# encoding=utf8
# author=spenly
# mail=i@spenly.com

from boto import sqs
from boto.sqs.message import Message
import json, time


class BigQueue():
    def __init__(self, queue_name, aws_access_key, aws_secret_key, aws_region="cn-north-1", visual_timeout=1):
        self.visual_timeout = 5
        try:
            self.conn = sqs.connect_to_region(aws_region, aws_access_key_id=aws_access_key,
                                              aws_secret_access_key=aws_secret_key)
            queue = self.conn.get_queue(queue_name)
            if not queue:
                queue = self.conn.create_queue(queue_name)
            self.queue = queue
        except Exception, ex:
            print ex

    def inqueue(self, dict_msg):
        """
        write a msg into queue
        :param json_msg: dict object
        :return:
        """
        str_msg = json.dumps(dict_msg)
        msg = Message()
        msg.set_body(str_msg)
        self.queue.write(msg)

    def dequeue(self):
        """
        get a msg from queue
        :return: a SQS Message type msg body
        """
        if self.count() == 0:
            return None
        msg = self.queue.read(self.visual_timeout)
        return msg

    def ack_message(self, msg):
        """
        acknowledge the message
        :param msg: SQS Message
        :return:
        """
        self.queue.delete_message(msg)

    def clear(self):
        """
        clear all msgs in queue
        :return:
        """
        self.queue.clear()

    def count(self):
        """
        get count of msgs in queue
        Notice: it is not a exactly number,
        just a approximate number,
        and will be exactly when the number is small.
        :return:
        """
        return self.queue.count()


if __name__ == "__main__":
    # test code
    access_key = "AKIAOTDCELIZTI4INNVA"
    secret_key = "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"
    bq = BigQueue("gemii_test", access_key, secret_key)
    if bq.queue:
        bq.clear()
        bq.inqueue(
            {"str": "abc", "num": "123", "chars": "中国abc123.com@\n测试", "time": time.strftime("%Y-%m-%d %H:%M:%S")})
        print bq.dequeue()
