import mq
import threading
import time
import json

MQ_ADDR = "amqp://192.168.0.38:5672"

# test info
queue_control = 'test.control'
queue_business = 'test.business'


class KeyboardThread(threading.Thread):
    def __init__(self, quitCallback):
        super(KeyboardThread, self).__init__()
        self._quitCallback = quitCallback

    def run(self):
        while True:
            cmd = raw_input('').lower()
            if cmd in ('quit', 'exit'):
                self._quitCallback()
                break
            elif cmd == 'start':
                data = {'command': 'start'}
                mq.callTopic(MQ_ADDR, queue_control, json.dumps(data))
            elif cmd == 'stop':
                data = {'command': 'stop'}
                mq.callTopic(MQ_ADDR, queue_control, json.dumps(data))
            else:
                print 'invalid command'


class MainThread(threading.Thread):
    def __init__(self):
        super(MainThread, self).__init__()
        self._listener = mq.MQNormalConsumer(MQ_ADDR, queue_business, self.onRecv)

    def onRecv(self, data):
        pass

    def run(self):
        print 'mainthread started.'
        self.running = True
        self._listener.start()
        while self.running:
            time.sleep(1)
        self._listener.stop()
        print 'mainthread stoped.'

    def stop(self):
        self.running = False


class Main():
    def __init__(self):
        KeyboardThread(self.onQuit).start()
        self._listener = mq.MQTopicConsumer(MQ_ADDR, queue_control, self.onRecv)
        self._listener.start()

        self._mainThread = None

    def onQuit(self):
        self._listener.stop()
        if self._mainThread:
            self._mainThread.stop()

    def onRecv(self, data):
        data = json.loads(data)
        func = getattr(self, 'recv_{command}'.format(command=data['command']))
        func(data)

    def recv_start(self, data):
        self._mainThread = MainThread()
        self._mainThread.start()

    def recv_stop(self, data):
        self._mainThread.stop()
        self._mainThread = None


if __name__ == '__main__':
    Main()
