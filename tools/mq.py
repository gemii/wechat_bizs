import pika
import uuid
import threading

EXCHANGE_TOPIC = 'gemii.topic'


class FibonacciRpcClient():
    def __init__(self, url):
        self.connection = pika.BlockingConnection(pika.URLParameters(url))

        self.channel = self.connection.channel()

        result = self.channel.queue_declare(exclusive=True)
        self.callback_queue = result.method.queue
        self.channel.basic_consume(self.on_response, no_ack=True,
                                   queue=self.callback_queue)

    def on_response(self, ch, method, props, body):
        if self.corr_id == props.correlation_id:
            self.response = body

    def call(self, queue, data):
        self.response = None
        self.corr_id = str(uuid.uuid4())
        self.channel.basic_publish(exchange='',
                                   routing_key=queue,
                                   properties=pika.BasicProperties(
                                       reply_to=self.callback_queue,
                                       correlation_id=self.corr_id,
                                   ),
                                   body=data)
        while self.response is None:
            self.connection.process_data_events()
        return self.response


class MQNormalConsumer(threading.Thread):
    def __init__(self, url, queue, callback):
        super(MQNormalConsumer, self).__init__()
        self._url = url
        self._queue = queue
        self._callback = callback

    def _connect(self):
        self._conn = pika.SelectConnection(pika.URLParameters(self._url),
                                           on_open_callback=self._onConnected,
                                           )

    def _onConnected(self, conn):
        self._conn.channel(self._onOpened)

    def _onOpened(self, channel):
        self._channel = channel
        self._channel.queue_declare(self._onQueueDeclared, queue=self._queue)

    def _onQueueDeclared(self, uf):
        self._tag = self._channel.basic_consume(self._onRecv, queue=self._queue, no_ack=False)

    def _onRecv(self, channel, deliver, properties, body):
        result = self._callback(body)
        corr_id = properties.correlation_id
        if corr_id:
            result = result or ''
            channel.basic_publish(exchange='',
                                  routing_key=properties.reply_to,
                                  properties=pika.BasicProperties(
                                      correlation_id=corr_id
                                  ),
                                  body=result)
        channel.basic_ack(deliver.delivery_tag)

    def run(self):
        self._connect()
        self._conn.ioloop.start()

    def stop(self):
        self._conn.close()


class MQTopicConsumer(threading.Thread):
    def __init__(self, url, queue, callback):
        super(MQTopicConsumer, self).__init__()
        self._url = url
        self._queue = queue
        self._callback = callback

    def _connect(self):
        self._conn = pika.SelectConnection(pika.URLParameters(self._url),
                                           on_open_callback=self._onConnected,
                                           )

    def _onConnected(self, conn):
        self._conn.channel(self._onOpened)

    def _onOpened(self, channel):
        self._channel = channel
        channel.exchange_declare(self._onExchangeDeclared,
                                 exchange=EXCHANGE_TOPIC,
                                 type='topic')

    def _onExchangeDeclared(self, uf):
        self._channel.queue_declare(self._onQueueDeclared, exclusive=True)

    def _onQueueDeclared(self, uf):
        self._bind_queue = uf.method.queue
        self._channel.queue_bind(self._onBind, queue=self._bind_queue,
                                 exchange=EXCHANGE_TOPIC,
                                 routing_key=self._queue)

    def _onBind(self, uf):
        self._channel.basic_consume(self._onRecv,
                                    queue=self._bind_queue,
                                    no_ack=True)

    def _onRecv(self, channel, deliver, properties, body):
        self._callback(body)

    def run(self):
        self._connect()
        self._conn.ioloop.start()

    def stop(self):
        self._conn.close()


def call(url, queue, data):
    conn = pika.BlockingConnection(pika.URLParameters(url))
    channel = conn.channel()
    channel.queue_declare(exclusive=True)
    channel.basic_publish(exchange='', routing_key=queue, body=data)
    conn.close()


def callRPC(url, queue, data):
    return FibonacciRpcClient(url).call(queue, data)


def callTopic(url, queue, data):
    conn = pika.BlockingConnection(pika.URLParameters(url))
    channel = conn.channel()
    channel.exchange_declare(exchange=EXCHANGE_TOPIC, type='topic')
    channel.basic_publish(exchange=EXCHANGE_TOPIC,
                          routing_key=queue,
                          body=data)
    conn.close()
