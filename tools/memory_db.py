# encoding=utf8

import redis


class MemoryDB(object):
    def __init__(self, host, port=6379, db=0, password=None):
        """
        Use redis as data container.
        Data structure:
            robot_id + item => set name
            key => key
            value => value
        :param host: redis host
        :param port: redis port, dafault 6379
        :param db: redis db, default 0
        :param password: redis password, default None
        """
        self.redis = redis.StrictRedis(host=host, port=port, db=db, password=password)

    def set_data(self, robot_id, item, key, value):
        """
        存放数据
        :param robot_id: 机器人编号 db
        :param item: 项目
        :param key: 键
        :param value: 值
        :return:
        """
        self.redis.hset(robot_id + "_" + item, key, value)

    def get_data(self, robot_id, item, key=None):
        """
        获取数据, 若key为空,则获取所有的key
        :param robot_id: 机器人编号
        :param item: 项目
        :param key: 键
        :return:
        """
        if key:
            # if key hits, will return your data object
            # else will return None
            return self.redis.hget(robot_id + "_" + item, key)
        else:
            # if set hits, will return your keys set
            # else will return empty keys list
            return self.redis.hkeys(robot_id + "_" + item)


if __name__ == "__main__":
    memory_db = MemoryDB("helper.gemii.cc", db=15, password="gemii@123.cc", port=8081)
    memory_db.set_data("1", "users", "g1", ["u1", "u2"])
    memory_db.set_data("1", "groups", "g1", {"name": "t1", "uc": 10})

    memory_db.set_data("2", "users", "g2", ["u1", "u2"])
    memory_db.set_data("2", "groups", "g2", {"name": "t1", "uc": 12})
    memory_db.set_data("2", "users", "g3", ["u1", "u2"])
    memory_db.set_data("2", "groups", "g3", {"name": "t1", "uc": 20})

    print memory_db.get_data("1", "users", "g1")
    print memory_db.get_data("1", "groups", "g1")
    print memory_db.get_data("1", "users")
    print memory_db.get_data("1", "groups")
